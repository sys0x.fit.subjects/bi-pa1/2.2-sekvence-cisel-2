cmake_minimum_required(VERSION 3.12)
project(DomaciUloha2_Tezsi C)

set(CMAKE_C_STANDARD 99)

add_executable(DomaciUloha2_Tezsi main.c)